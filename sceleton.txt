Sceleton and basic packages

├── client              //network layer
│   ├── http		        //http client package
│   │     └─ httpClient.go
│   └── websockets	        //websockets client package
│         └─ wsClient.go
│
├── presentation        //presentation layer
│
│
├── model               //Data Transfer Objects mapping
│   ├── message.go		    //message data structure
│   └── user.go             //used data structure
│
└── main.go
