package http

import (
	"net/http"
	"log"
	"fmt"
)

func Example() {
	resp, err := http.Get("http://example.com/")

	if err != nil {
		log.Fatal(err)
	}

	defer resp.Body.Close()

	fmt.Printf("%s", resp.Status)
}